
## Intro
Thanks for taking the time to review my tech test, please visit [this](https://www.youtube.com/watch?v=NPza2hYf0SE) YouTube video for a screenshare walkthrough of the code.

## Installation
The only thing you'll need to get the project up and running is Cocoapods. If you have that installed, simply cd into the project folder in your terminal and run `pod install`.
