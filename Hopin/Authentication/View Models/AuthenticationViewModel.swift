//
//  AuthenticationViewModel.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

class AuthenticationViewModel {
    typealias ChangeHandler = (Change) -> Void
    var didChange: ChangeHandler?
    
    func loadData() {
        didChange?(.loading(true))
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 2.0) {
            self.didChange?(.loggedOut)
            self.didChange?(.loading(false))
        }
    }
}

extension AuthenticationViewModel {
    enum Change {
        case loggedIn
        case loggedOut
        case loading(Bool)
    }
}
