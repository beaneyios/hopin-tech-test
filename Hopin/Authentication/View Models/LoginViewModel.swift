//
//  LoginViewModel.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

class LoginViewModel {
    typealias ChangeHandler = (Change) -> Void
    var didChange: ChangeHandler?
    
    let client: Authenticating
    let sessionManager: SessionStoring
    let cookieManager: CookieManaging
    
    init(
        client: Authenticating = AuthenticationClient(),
        sessionManager: SessionStoring = SessionManager(),
        cookieManager: CookieManaging = CookieManager()
    ) {
        self.client = client
        self.sessionManager = sessionManager
        self.cookieManager = cookieManager
    }
    
    func createSession() {
        cookieManager.cookie(named: "user.token") { [weak self] cookie in
            self?.createSession(with: cookie)
        }
    }
    
    private func createSession(with cookie: HTTPCookie?) {
        guard let cookie = cookie else {
            return
        }
        
        client.createSessionToken(slug: Slugs.mine, token: cookie.value) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case let .success(session):
                self.sessionManager.store(session: session)
                self.didChange?(.loggedIn(session))
            case let .failure(error):
                self.didChange?(.errored(error))
            }
        }
    }
}

extension LoginViewModel {
    enum Change {
        case loggedIn(Session)
        case loading(Bool)
        case errored(Error)
    }
}
