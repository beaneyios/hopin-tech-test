//
//  AuthenticationClient.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

protocol Authenticating {
    typealias SessionCreationHandler = (Result<Session, Error>) -> Void
    func createSessionToken(
        slug: String,
        token: String,
        completion: @escaping SessionCreationHandler
    )
}

class AuthenticationClient: Authenticating {
    let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func createSessionToken(
        slug: String,
        token: String,
        completion: @escaping SessionCreationHandler
    ) {
        var request = URLRequest(url: URLs.createSession)
        request.httpMethod = "POST"
        request.httpBody = "{\"event_slug\":\"\(slug)\"}".data(using: .utf8)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("user.token=\(token)", forHTTPHeaderField: "Cookie")
        
        session.dataTaskWithRequest(req: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            
            do {
                let response = try decoder.decode(Session.self, from: data)
                completion(.success(response))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
}
