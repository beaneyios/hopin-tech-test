//
//  SessionStoring.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

protocol SessionStoring {
    func store(session: Session)
    func fetchSession() -> Session?
    func clearSession()
}

class SessionManager: SessionStoring {
    // TODO: Implement using keychain
    func store(session: Session) {
        
    }
    
    func fetchSession() -> Session? {
        nil
    }
    
    func clearSession() {
        
    }
}
