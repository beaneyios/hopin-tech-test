//
//  CookieManager.swift
//  Hopin
//
//  Created by Matt Beaney on 28/11/2021.
//

import WebKit

protocol CookieManaging {
    func cookie(named name: String, completion: @escaping (HTTPCookie?) -> Void)
    func clearCookies(completion: @escaping () -> Void)
}

class CookieManager: CookieManaging {
    func cookie(named name: String, completion: @escaping (HTTPCookie?) -> Void) {
        let store = WKWebsiteDataStore.default()
        store.httpCookieStore.getAllCookies { cookies in
            let userCookie = cookies.first { cookie in
                cookie.name == name
            }
            
            completion(userCookie)
        }
    }
    
    func clearCookies(completion: @escaping () -> Void) {
        let store = WKWebsiteDataStore.default()
        let cookieStore = store.httpCookieStore
        cookieStore.getAllCookies { cookies in
            let group = DispatchGroup()
            cookies.forEach { cookie in
                group.enter()
                cookieStore.delete(cookie) {
                    group.leave()
                }
            }
            
            group.notify(queue: .global()) {
                completion()
            }
        }
    }
}
