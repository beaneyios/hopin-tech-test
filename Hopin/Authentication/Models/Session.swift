//
//  SessionResponse.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

struct Session: Codable {
    let token: String
}
