//
//  AuthenticationCoordinator.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit

protocol AuthenticationCoordinatorDelegate: AnyObject {
    func authenticationCoordinatorDidLogin(session: Session)
    func authenticationCoordinatorDidFinish(_ coordinator: AuthenticationCoordinator)
}

class AuthenticationCoordinator: ViewCoordinator {
    weak var delegate: AuthenticationCoordinatorDelegate?
    
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    override func start() {
        navigateToAuthentication()
    }
    
    private func navigateToAuthentication() {
        let viewModel = AuthenticationViewModel()
        let viewController = AuthenticationViewController(viewModel: viewModel)
        viewController.delegate = self
        navigationController.setViewControllers([viewController], animated: true)
    }
    
    private func navigateToLogin() {
        let viewModel = LoginViewModel()
        let viewController = LoginViewController(viewModel: viewModel)
        viewController.delegate = self
        navigationController.setViewControllers([viewController], animated: true)
    }
}

extension AuthenticationCoordinator: AuthenticationViewControllerDelegate {
    func authenticationViewControllerLoggedOut(_ viewController: AuthenticationViewController) {
        navigateToLogin()
    }
    
    func authenticationViewController(
        _ viewController: AuthenticationViewController,
        didLoginWithSession session: Session
    ) {
        delegate?.authenticationCoordinatorDidLogin(session: session)
        delegate?.authenticationCoordinatorDidFinish(self)
    }
}

extension AuthenticationCoordinator: LoginViewControllerDelegate {
    func loginViewController(
        _ viewController: LoginViewController,
        didLoginWithSession session: Session
    ) {
        delegate?.authenticationCoordinatorDidLogin(session: session)
        delegate?.authenticationCoordinatorDidFinish(self)
    }
}
