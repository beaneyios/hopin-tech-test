//
//  AuthenticationViewController.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit

protocol AuthenticationViewControllerDelegate: AnyObject {
    func authenticationViewControllerLoggedOut(_ viewController: AuthenticationViewController)
    func authenticationViewController(
        _ viewController: AuthenticationViewController,
        didLoginWithSession session: Session
    )
}

class AuthenticationViewController: UIViewController, LoadingPresenting {
    @IBOutlet weak var loadingSpinnerContainer: UIView!
    var loadingSpinner: HopinLoaderView!
    
    weak var delegate: AuthenticationViewControllerDelegate?
    
    let viewModel: AuthenticationViewModel
    
    init(viewModel: AuthenticationViewModel) {
        self.viewModel = viewModel
        super.init(
            nibName: String(describing: Self.self),
            bundle: nil
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLoader()
        observeViewModel()
        viewModel.loadData()
    }
    
    private func observeViewModel() {
        viewModel.didChange = { change in
            DispatchQueue.main.async {
                self.handleChange(change)
            }
        }
    }
    
    private func handleChange(_ change: AuthenticationViewModel.Change) {
        switch change {
        case .loggedOut:
            self.delegate?.authenticationViewControllerLoggedOut(self)
        case .loggedIn:
            break
        case let .loading(isLoading):
            if isLoading {
                self.startLoading()
            } else {
                self.stopLoading()
            }
        }
    }
}
