//
//  LoginViewController.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit
import WebKit

protocol LoginViewControllerDelegate: AnyObject {
    func loginViewController(
        _ viewController: LoginViewController,
        didLoginWithSession session: Session
    )
}

class LoginViewController: UIViewController, LoadingPresenting {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loadingSpinnerContainer: UIView!
    var loadingSpinner: HopinLoaderView!
    
    weak var delegate: LoginViewControllerDelegate?
    
    let viewModel: LoginViewModel
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(
            nibName: String(describing: Self.self),
            bundle: nil
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLoader()
        configureWebView()
        loadLoginPage()
        observeViewModel()
    }
    
    private func observeViewModel() {
        viewModel.didChange = { change in
            DispatchQueue.main.async {
                self.handleViewModelChange(change)
            }
        }
    }
    
    private func handleViewModelChange(_ change: LoginViewModel.Change) {
        switch change {
        case let .loggedIn(session):
            delegate?.loginViewController(self, didLoginWithSession: session)
        case let .loading(isLoading):
            if isLoading {
                startLoading()
            } else {
                stopLoading()
            }
            break
        case .errored(_):
            break
        }
    }
    
    private func configureWebView() {
        webView.navigationDelegate = self
        webView.configuration.defaultWebpagePreferences.allowsContentJavaScript = true
    }

    private func loadLoginPage() {
        let request = URLRequest(url: URLs.login)
        startLoading()
        webView.load(request)
    }
}

extension LoginViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView.url == URLs.account {
            viewModel.createSession()
        }
    }
    
    func webView(
        _ webView: WKWebView,
        decidePolicyFor navigationAction: WKNavigationAction
    ) async -> WKNavigationActionPolicy {
        if navigationAction.request.url == URLs.account {
            webView.isHidden = true
            startLoading()
        }
        
        if webView.url == URLs.login {
            stopLoading()
        }
        
        return .allow
    }
}
