//
//  HopinLoader.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit

class HopinLoaderView: UIView, NibLoadable {
    @IBOutlet weak var imgView: UIImageView!
    private var isLoading = false
    
    func startLoading() {
        isLoading = true
        rotate()
    }
    
    func stopLoading() {
        isLoading = false
    }
    
    private func rotate() {
        guard isLoading else {
            return
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear) {
            self.imgView.transform = self.imgView.transform.rotated(by: .pi)
        } completion: { _ in
            self.rotate()
        }
    }
}
