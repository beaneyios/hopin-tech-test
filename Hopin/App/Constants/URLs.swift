//
//  URLs.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

struct URLs {
    static let baseUrl = URL(string: "https://hopin.com")!
    static let login = baseUrl.appendingPathComponent("/sign_in")
    static let account = baseUrl.appendingPathComponent("/account")
    static let createSession = baseUrl.appendingPathComponent("/users").appendingPathComponent("sso")
    
    private static let apiEndpoint = baseUrl.appendingPathComponent("/api").appendingPathComponent("/v2")
    static let events = apiEndpoint.appendingPathComponent("/events")
        
    static func stages(for id: Int) -> URL {
        event(eventId: id).appendingPathComponent("/stages")
    }
    
    static func status(eventId: Int, uuid: String) -> URL {
        event(eventId: eventId)
            .appendingPathComponent("/studio")
            .appendingPathComponent("/\(uuid)")
            .appendingPathComponent("/status")
    }
    
    private static func event(eventId: Int) -> URL {
        events.appendingPathComponent("\(eventId)")
    }
}
