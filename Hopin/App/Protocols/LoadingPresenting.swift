//
//  LoadingPresenting.swift
//  Hopin
//
//  Created by Matt Beaney on 28/11/2021.
//

import UIKit

protocol LoadingPresenting: AnyObject {
    var loadingSpinnerContainer: UIView! { get }
    var loadingSpinner: HopinLoaderView! { get set }
}

extension LoadingPresenting {
    func configureLoader() {
        let loadingSpinner = HopinLoaderView.createFromNib()
        loadingSpinner.translatesAutoresizingMaskIntoConstraints = false
        loadingSpinnerContainer.addSubview(loadingSpinner)
        loadingSpinner.pin(to: loadingSpinnerContainer)
        self.loadingSpinner = loadingSpinner
    }
    
    func startLoading() {
        self.loadingSpinner.isHidden = false
        self.loadingSpinner?.startLoading()
    }
    
    func stopLoading() {
        self.loadingSpinner.isHidden = true
        self.loadingSpinner?.stopLoading()
    }
}
