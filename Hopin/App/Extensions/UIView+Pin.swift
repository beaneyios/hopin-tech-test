//
//  UIView+Pin.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit

extension UIView {
    func pin(to parent: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: parent.leadingAnchor),
            trailingAnchor.constraint(equalTo: parent.trailingAnchor),
            topAnchor.constraint(equalTo: parent.topAnchor),
            bottomAnchor.constraint(equalTo: parent.bottomAnchor)
        ])
    }
}
