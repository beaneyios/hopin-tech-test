//
//  URLSession+URLSessionProtocol.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

extension URLSession : URLSessionProtocol {
    public func dataTaskWithRequest(
        req: URLRequest,
        completionHandler: @escaping DataTaskResult
    ) -> URLSessionDataTaskProtocol {
        return (
            dataTask(
                with: req,
                completionHandler: completionHandler
            ) as URLSessionDataTask
        ) as URLSessionDataTaskProtocol
    }
    
    public func dataTaskWithURL(
        url: URL,
        completionHandler: @escaping DataTaskResult
    ) -> URLSessionDataTaskProtocol {
        return (
            dataTask(
                with: url,
                completionHandler: completionHandler
            ) as URLSessionDataTask
        ) as URLSessionDataTaskProtocol
    }
}
extension URLSessionDataTask : URLSessionDataTaskProtocol {}
