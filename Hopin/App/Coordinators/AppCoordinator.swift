//
//  AppCoordinator.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation
import UIKit

class AppCoordinator: ViewCoordinator {
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    override func start() {
        navigateToAuthentication()
    }
    
    private func navigateToAuthentication() {
        let coordinator = AuthenticationCoordinator(navigationController: navigationController)
        coordinator.delegate = self
        add(coordinator)
        coordinator.start()
    }
    
    private func navigateToStream(session: Session) {
        let coordinator = StreamCoordinator(
            navigationController: navigationController,
            session: session
        )
        
        coordinator.delegate = self
        add(coordinator)
        coordinator.start()
    }
}

extension AppCoordinator: AuthenticationCoordinatorDelegate {
    func authenticationCoordinatorDidLogin(session: Session) {
        navigateToStream(session: session)
    }
    
    func authenticationCoordinatorDidFinish(_ coordinator: AuthenticationCoordinator) {
        remove(coordinator)
    }
}

extension AppCoordinator: StreamCoordinatorDelegate {
    func streamCoordinatorDidLogout(_ coordinator: StreamCoordinator) {
        navigateToAuthentication()
    }
    
    func streamCoordinatorDidFinish(_ coordinator: StreamCoordinator) {
        remove(coordinator)
    }
}
