//
//  Array+Videos.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

extension Array where Element == Video {
    var activeVideo: Video? {
        first { video in
            let correctType = video.deliveryType == .hopin || video.deliveryType == .ivs
            let correctStatus = video.status == .active
            return correctType && correctStatus
        }
    }
}
