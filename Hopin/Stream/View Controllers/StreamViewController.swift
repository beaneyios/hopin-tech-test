//
//  StreamViewController.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit
import AVKit

protocol StreamViewControllerDelegate: AnyObject {
    func streamViewControllerDidTapLogout(_ viewController: StreamViewController)
}

class StreamViewController: UIViewController {
    @IBOutlet weak var videoContainer: UIView!
    @IBOutlet weak var logoutButton: UIButton!
    
    weak var delegate: StreamViewControllerDelegate?
    
    let viewModel: StreamViewModel
    
    init(viewModel: StreamViewModel) {
        self.viewModel = viewModel
        super.init(
            nibName: String(describing: Self.self),
            bundle: nil
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeViewModel()
        configureLogoutButton()
        viewModel.loadData()
    }
    
    @IBAction func didTapLogout(_ sender: Any) {
        viewModel.logout()
    }
    
    private func configureLogoutButton() {
        logoutButton.layer.cornerRadius = 5.0
        logoutButton.layer.borderColor = UIColor.red.cgColor
        logoutButton.layer.borderWidth = 2.0
    }
    
    private func renderVideo(url: URL) {
        let asset = AVAsset(url: url)
        let playerItem = AVPlayerItem(asset: asset)
        let player = AVPlayer(playerItem: playerItem)
        
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        playerViewController.willMove(toParent: self)
        addChild(playerViewController)
        playerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        videoContainer.addSubview(playerViewController.view)
        playerViewController.view.pin(to: videoContainer)
        playerViewController.didMove(toParent: self)
        player.play()
    }
    
    private func observeViewModel() {
        viewModel.didChange = { change in
            DispatchQueue.main.async {
                self.handleChange(change)
            }
        }
    }
    
    private func handleChange(_ change: StreamViewModel.Change) {
        switch change {
        case .loading(_):
            break
        case let .liveStreamUrlLoaded(streamUrl):
            self.renderVideo(url: streamUrl)
        case .errored(_):
            break
        case .loggedOut:
            delegate?.streamViewControllerDidTapLogout(self)
        }
    }
}
