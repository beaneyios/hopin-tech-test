//
//  Video.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

enum Status: String, Codable {
    case active
    case inactive
}

enum DeliveryType: String, Codable {
    case hopin
    case ivs
    case mux
}

struct VideoResponse: Codable {
    let videoChannels: [Video]
}

struct Video: Codable {
    let streamUrl: URL
    let status: Status
    let deliveryType: DeliveryType
}
