//
//  Stage.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

struct StageResponse: Codable {
    let stages: [Stage]
}

struct Stage: Codable {
    let uuid: String
}
