//
//  SessionDecrypter.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit
import JWTDecode

protocol SessionDecrypting {
    func eventId(from session: Session) -> Int?
}

class SessionDecrypter: SessionDecrypting {
    func eventId(from session: Session) -> Int? {
        do {
            let jwt = try decode(jwt: session.token)
            return jwt.body["event_id"] as? Int
        } catch {
            return nil
        }
    }
}
