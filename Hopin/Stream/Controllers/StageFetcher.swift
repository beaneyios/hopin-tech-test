//
//  StageFetcher.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

protocol StageFetching {
    typealias StageCompletionHandler = (Result<StageResponse, Error>) -> Void
    func fetchStage(
        eventId: Int,
        token: String,
        completion: @escaping StageCompletionHandler
    )
}

class StageFetcher: StageFetching {
    let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func fetchStage(
        eventId: Int,
        token: String,
        completion: @escaping StageCompletionHandler
    ) {
        var request = URLRequest(url: URLs.stages(for: eventId))
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        session.dataTaskWithRequest(req: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            
            do {
                let response = try decoder.decode(
                    StageResponse.self,
                    from: data
                )
                completion(.success(response))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
}
