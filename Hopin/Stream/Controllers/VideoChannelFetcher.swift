//
//  VideoChannelFetcher.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation

protocol VideoChannelFetching {
    typealias VideoCompletionHandler = (Result<VideoResponse, Error>) -> Void
    func fetchVideoChannels(
        eventId: Int,
        uuid: String,
        token: String,
        completion: @escaping VideoCompletionHandler
    )
}

class VideoChannelFetcher: VideoChannelFetching {
    let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func fetchVideoChannels(
        eventId: Int,
        uuid: String,
        token: String,
        completion: @escaping VideoCompletionHandler
    ) {
        var request = URLRequest(url: URLs.status(eventId: eventId, uuid: uuid))
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        session.dataTaskWithRequest(req: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                return
            }
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            
            do {
                let response = try decoder.decode(
                    VideoResponse.self,
                    from: data
                )
                completion(.success(response))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
}
