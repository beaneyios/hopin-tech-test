//
//  StreamCoordinator.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import UIKit

protocol StreamCoordinatorDelegate: AnyObject {
    func streamCoordinatorDidLogout(_ coordinator: StreamCoordinator)
    func streamCoordinatorDidFinish(_ coordinator: StreamCoordinator)
}

class StreamCoordinator: ViewCoordinator {
    weak var delegate: StreamCoordinatorDelegate?
    
    let navigationController: UINavigationController
    let session: Session
    
    init(navigationController: UINavigationController, session: Session) {
        self.navigationController = navigationController
        self.session = session
    }
    
    override func start() {
        navigateToStream()
    }
    
    private func navigateToStream() {
        let viewModel = StreamViewModel(session: session)
        let viewController = StreamViewController(viewModel: viewModel)
        viewController.delegate = self
        navigationController.setViewControllers([viewController], animated: true)
    }
}

extension StreamCoordinator: StreamViewControllerDelegate {
    func streamViewControllerDidTapLogout(_ viewController: StreamViewController) {
        delegate?.streamCoordinatorDidLogout(self)
        delegate?.streamCoordinatorDidFinish(self)
    }
}
