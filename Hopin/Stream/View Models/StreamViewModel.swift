//
//  StreamViewModel.swift
//  Hopin
//
//  Created by Matt Beaney on 27/11/2021.
//

import Foundation
import WebKit

class StreamViewModel {
    typealias ChangeHandler = (Change) -> Void
    var didChange: ChangeHandler?
    
    private let session: Session
    private let decrypter: SessionDecrypting
    private let stageFetcher: StageFetching
    private let videoFetcher: VideoChannelFetching
    private let sessionManager: SessionStoring
    private let cookieManager: CookieManaging
    
    init(
        session: Session,
        decrypter: SessionDecrypting = SessionDecrypter(),
        stageFetcher: StageFetching = StageFetcher(),
        videoFetcher: VideoChannelFetching = VideoChannelFetcher(),
        sessionManager: SessionStoring = SessionManager(),
        cookieManager: CookieManaging = CookieManager()
    ) {
        self.session = session
        self.decrypter = decrypter
        self.stageFetcher = stageFetcher
        self.videoFetcher = videoFetcher
        self.sessionManager = sessionManager
        self.cookieManager = cookieManager
    }
    
    func loadData() {
        guard let eventId = decrypter.eventId(from: session) else {
            return
        }
        
        stageFetcher.fetchStage(
            eventId: eventId,
            token: session.token
        ) { [weak self] result in
            switch result {
            case let .success(response):
                self?.handleStages(
                    response.stages,
                    eventId: eventId
                )
            case let .failure(error):
                self?.didChange?(.errored(error))
            }
        }
    }
    
    func logout() {
        sessionManager.clearSession()
        cookieManager.clearCookies {
            self.didChange?(.loggedOut)
        }
    }
    
    private func handleStages(_ stages: [Stage], eventId: Int) {
        guard let firstStage = stages.first else {
            return
        }
        
        videoFetcher.fetchVideoChannels(
            eventId: eventId,
            uuid: firstStage.uuid,
            token: session.token
        ) { [weak self] result in
            switch result {
            case let .success(response):
                self?.handleVideos(response.videoChannels)
            case let .failure(error):
                self?.didChange?(.errored(error))
            }
        }
    }
    
    private func handleVideos(_ videos: [Video]) {
        guard let activeVideo = videos.activeVideo else {
            return
        }
        
        didChange?(.liveStreamUrlLoaded(activeVideo.streamUrl))
    }
}

extension StreamViewModel {
    enum Change {
        case loading(Bool)
        case liveStreamUrlLoaded(URL)
        case errored(Error)
        case loggedOut
    }
}
